#include "BH1750Lib/BH1750Lib.h"

#define UARTDEBUG 1

BH1750Lib bh1750;

void setup() {
    bh1750.begin(BH1750LIB_MODE_CONTINUOUSHIGHRES);
  
#if UARTDEBUG == 1
    Serial.begin(9600);
    Serial.println("Starting...");
#endif
}

String Past_Status = "";
String Light_Status = "";

void loop() {
    uint16_t luxvalue = bh1750.lightLevel();

    if (luxvalue >= 5000) {
        Light_Status = "sun";
    }

    if (luxvalue < 5000) {
        Light_Status = "no_sun";
    }
    
    if (Light_Status != Past_Status) {
        Particle.publish("Light_Status", Light_Status);
    }
    
    Past_Status = Light_Status;

    delay(2000);
     
}
